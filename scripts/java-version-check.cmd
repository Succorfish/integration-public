@echo off
set java_old_versions_pattern=1.
java -version 2> outputfile1.txt

set /p text=< outputfile.txt

for /f ^"tokens^=1^ delims^=^"^" %%a in (^"%text:*"=%^") do (
	set result=%%a
)

echo %text% | findstr /C:version 1>nul

if errorlevel 1 (
  echo JDK not found on your machine! JDK 11+ MUST BE installed and added to the PATH variable.
) ELSE (
  if %result:~0,2%==%java_old_versions_pattern% (
  	for /f "tokens=2 delims=." %%a in ("%result%") do (
  		echo Current version of Java JDK is %%a
  		echo Minimum reqired JDK version is 11. Please, update your JDK version and use this check again!
  	)
  ) ELSE (
  	for /f "tokens=1 delims=." %%a in ("%result%") do (
  		if %%a GEQ 11 (
  			echo Current version of Java JDK is %%a
  			echo This JDK is compatible with applications in this repository.
  		) ELSE (
  			echo Current version of Java: %%a
  			echo Minimum reqired JDK version is 11. Please, update your JDK version and use this check again!
  		)
  	)
  )
)

del outputfile1.txt