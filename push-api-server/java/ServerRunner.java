import com.sun.net.httpserver.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.Objects;
import java.util.Properties;
import java.util.Scanner;

public class ServerRunner {
	// Properties
	private static final String SERVER_PORT = "serverPort";
	private static final String USERNAME = "authUsername";
	private static final String PASSWORD = "authPassword";
	// conf files locations
	private static final String PROPERTIES_FILE = "application.properties";

	public static void main(String[] args) throws IOException {
		Properties properties = new Properties();
		// load a properties file from class path
		properties.load(Objects.requireNonNull(ServerRunner.class.getResourceAsStream(PROPERTIES_FILE)));
		// get the property values
		int serverPort = Integer.parseInt(properties.getProperty(SERVER_PORT));
		String username = properties.getProperty(USERNAME);
		String password = properties.getProperty(PASSWORD);

		HttpServer server = HttpServer.create(new InetSocketAddress(serverPort), 0);
		HttpContext hc1 = server.createContext("/", new GetHandler());
		hc1.setAuthenticator(new BasicAuthenticator("realm") {
			@Override
			public boolean checkCredentials(String user, String pwd) {
				return user.equals(username) && pwd.equals(password);
			}
		});
		server.start();
		System.out.println("The server is running...");
	}

	static class GetHandler implements HttpHandler {
		public void handle(HttpExchange httpExchange) throws IOException {
			String response = "<html><body>Login Success</body></html>";
			printRequestInfo(httpExchange);
			writeResponse(httpExchange, response);
		}
	}

	private static void writeResponse(HttpExchange httpExchange, String response) throws IOException {
		httpExchange.sendResponseHeaders(200, response.length());
		OutputStream os = httpExchange.getResponseBody();
		os.write(response.getBytes());
		os.close();
	}

	private static void printRequestInfo(HttpExchange exchange) {
		System.out.println("-- Received Data --");
		InputStream requestBody = exchange.getRequestBody();
		Scanner scanner = new Scanner(requestBody);
		StringBuilder body = new StringBuilder();
		while (scanner.hasNext()) {
			body.append(scanner.nextLine());
		}
		System.out.println(body);
	}
}
