# Push API Java Server

This is Java 11+ Push API server.

## Running directives
- Update `application.properties` file with your server properties
- Share your server static URL (e.g. http://<IP/Domain>:<Port>) with administrators
- Share your server auth credentials with administrators
- Run `java ServerRunner.java` to start server
