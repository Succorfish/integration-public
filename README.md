# Succorfish Public Integration Repository

This repository is the main resource for systems integrators / developers integrating with Succorfish systems. It contains code samples of Succorfish API client applications. 
Systems integrators / developers can use these samples to run, test, extend, or build their own API clients by example.

## List of API Client Applications

1. Import API client. Used to import third-party data into Succorfish system.
2. Pull API client. Client application that pulls data that already has been queued for particular subscriber.
3. Push API server. Local server that receives data from Succorfish Push API in real-time and further disseminate data if needed.

See more details in respective folders.

Note: Please check your Java version before proceeding. To do that, please execute in this directory: 

```bash
user@server:~$ sh ./scripts/java-version-check.sh
```