# Import API Java Client

This is Import API Java 11+ client. It's simplified and only contain runnable source Java file, configuration .properties
file. 'keystore' folder should be create alongside /java folder and contain .p12 certificate key.

## Running directives
- Put your .p12 key into `/import-api-client/keystore` folder
- Update `application.properties` file with your key credentials
- Update files in `/import-api-client/sample` if needed 
- Run `java Runner.java help` for detailed instructions
