import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import java.io.*;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.util.*;
import java.util.stream.Collectors;

public class Runner {
	// Properties
	private static final String CHECK_AUTH_URL = "checkUrl";
	private static final String LOAD_BINARY_DATA_URL = "binaryDataUrl";
	private static final String LOAD_WAYPOINT_URL = "waypointUrl";
	private static final String LOAD_ASSISTANCE_SWITCH_EVENT_URL = "assistanceSwitchEventUrl";
	private static final String LOAD_BATTERY_EVENT_URL = "batteryEventUrl";
	private static final String LOAD_CHARGING_EVENT_URL = "chargingEventUrl";
	private static final String LOAD_ENCLOSURE_TAMPER_EVENT_URL = "enclosureTamperEventUrl";
	private static final String LOAD_RESET_EVENT_URL = "resetEventUrl";
	private static final String LOAD_TAMPER_LOOP_CABLE_EVENT_URL = "tamperLoopCableEventUrl";
	private static final String LOAD_RFID_EVENT_URL = "rfidEventUrl";
	private static final String LOAD_BEACON_EVENT_URL = "beaconEventUrl";
	private static final String LOAD_GPS_CONNECTIVITY_TIMEOUT_EVENT_URL = "gpsConnectivityTimeoutEventUrl";
	private static final String LOAD_SATELLITE_CONNECTIVITY_TIMEOUT_EVENT_URL = "satelliteConnectivityTimeoutEventUrl";
	private static final String LOAD_CELULLAR_CONNECTIVITY_TIMEOUT_EVENT_URL = "celullarConnectivityTimeoutEventUrl";
	private static final String LOAD_ANTENNA_STATE_EVENT_URL = "antennaStateEventUrl";
	private static final String KEY_PASS = "keyPass";
	private static final String KEY_FILE_PATH = "keyFile";
	// Configuration constants
	private static final String PKCS12 = "PKCS12";
	private static final String TLS = "TLS";
	private static final int ERROR_STATUS_CODE = 429;
	// Parser constants
	private static final char COMMA = ',';
	private static final String DELIMITER = "(?<=},)";
	// Arguments
	private static final int MAIN_ARGUMENT_INDEX = 0;
	private static final int DATA_TYPE_ARGUMENT_INDEX = 1;
	private static final int EVENT_TYPE_ARGUMENT_INDEX = 2;
	private static final String HELP_ARGUMENT = "help";
	private static final String CHECK_ARGUMENT = "check";
	private static final String TEST_ARGUMENT = "test";
	private static final String WAYPOINT_ARGUMENT = "wp";
	private static final String BINARY_DATA_ARGUMENT = "bin";
	private static final String EVENT_ARGUMENT = "ev";
	private static final String ASSISTANCE_SWITCH_EVENT_ARGUMENT = "assis";
	private static final String BATTERY_EVENT_ARGUMENT = "battr";
	private static final String CHARGING_EVENT_ARGUMENT = "power";
	private static final String ENCLOSURE_TAMPER_EVENT_ARGUMENT = "encls";
	private static final String RESET_EVENT_ARGUMENT = "reset";
	private static final String TAMPER_LOOP_CABLE_EVENT_ARGUMENT = "cable";
	private static final String RFID_EVENT_ARGUMENT = "rfid";
	private static final String BEACON_EVENT_ARGUMENT = "beacn";
	private static final String GPS_CONNECTIVITY_TIMEOUT_EVENT_ARGUMENT = "gpsti";
	private static final String SATELLITE_CONNECTIVITY_TIMEOUT_EVENT_ARGUMENT = "satti";
	private static final String CELULLAR_CONNECTIVITY_TIMEOUT_EVENT_ARGUMENT = "celti";
	private static final String ANTENNA_STATE_EVENT_ARGUMENT = "anten";
	// conf files locations
	private static final String HELP_FILE = "help.txt";
	private static final String PROPERTIES_FILE = "application.properties";
	// test samples files locations
	private static final String BINARY_DATA_SAMPLE = "../sample/binary-data.json";
	private static final String WAYPOINT_SAMPLE = "../sample/waypoint.json";
	private static final String ASSISTANCE_SWITCH_EVENT_SAMPLE = "../sample/assistance-switch-event.json";
	private static final String BATTERY_EVENT_SAMPLE = "../sample/battery-event.json";
	private static final String CHARGING_EVENT_SAMPLE = "../sample/charging-event.json";
	private static final String ENCLOSURE_TAMPER_EVENT_SAMPLE = "../sample/enclosure-tamper-event.json";
	private static final String RESET_EVENT_SAMPLE = "../sample/reset-event.json";
	private static final String TAMPER_LOOP_CABLE_EVENT_SAMPLE = "../sample/tamper-loop-cable-event.json";
	private static final String RFID_EVENT_SAMPLE = "../sample/rfid-event.json";
	private static final String BEACON_EVENT_SAMPLE = "../sample/beacon-event.json";
	private static final String GPS_CONNECTIVITY_TIMEOUT_EVENT_SAMPLE = "../sample/gps-connectivity-timeout-event.json";
	private static final String SATELLITE_CONNECTIVITY_TIMEOUT_EVENT_SAMPLE = "../sample/satellite-connectivity-timeout-event.json";
	private static final String CELULLAR_CONNECTIVITY_TIMEOUT_EVENT_SAMPLE = "../sample/celullar-connectivity-timeout-event.json";
	private static final String ANTENNA_STATE_EVENT_SAMPLE = "../sample/antenna-state-event.json";

	public static void main(String[] args) throws Exception {
		if (args.length == 1 && HELP_ARGUMENT.equals(args[MAIN_ARGUMENT_INDEX])) {
			// 'help'
			try {
				Scanner scanner = new Scanner(new File(HELP_FILE));
				while (scanner.hasNextLine()) {
					System.out.println(scanner.nextLine());
				}
				scanner.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		} else {
			Properties properties = new Properties();
			// load a properties file from class path
			properties.load(Objects.requireNonNull(Runner.class.getResourceAsStream(PROPERTIES_FILE)));

			// get the property values
			URI checkAuthUri = URI.create(properties.getProperty(CHECK_AUTH_URL));
			URI loadWaypointUri = URI.create(properties.getProperty(LOAD_WAYPOINT_URL));
			URI loadBinaryDataUri = URI.create(properties.getProperty(LOAD_BINARY_DATA_URL));
			URI loadAssistanceSwitchEventUri = URI.create(properties.getProperty(LOAD_ASSISTANCE_SWITCH_EVENT_URL));
			URI loadBatteryEventUri = URI.create(properties.getProperty(LOAD_BATTERY_EVENT_URL));
			URI loadChargingEventUri = URI.create(properties.getProperty(LOAD_CHARGING_EVENT_URL));
			URI loadEnclosureTamperEventUri = URI.create(properties.getProperty(LOAD_ENCLOSURE_TAMPER_EVENT_URL));
			URI loadResetEventUri = URI.create(properties.getProperty(LOAD_RESET_EVENT_URL));
			URI loadTamperLoopCableEventUri = URI.create(properties.getProperty(LOAD_TAMPER_LOOP_CABLE_EVENT_URL));
			URI loadRfidEventUri = URI.create(properties.getProperty(LOAD_RFID_EVENT_URL));
			URI loadBeaconEventUri = URI.create(properties.getProperty(LOAD_BEACON_EVENT_URL));
			URI loadGpsConnectivityTimeoutEventUri = URI.create(properties.getProperty(LOAD_GPS_CONNECTIVITY_TIMEOUT_EVENT_URL));
			URI loadSatelliteConnectivityTimeoutEventUri = URI.create(properties.getProperty(LOAD_SATELLITE_CONNECTIVITY_TIMEOUT_EVENT_URL));
			URI loadCelullarConnectivityTimeoutEventUri = URI.create(properties.getProperty(LOAD_CELULLAR_CONNECTIVITY_TIMEOUT_EVENT_URL));
			URI loadAntennaStateEventUri = URI.create(properties.getProperty(LOAD_ANTENNA_STATE_EVENT_URL));
			String keyPass = properties.getProperty(KEY_PASS);
			String keyFile = properties.getProperty(KEY_FILE_PATH);

			// configuration
			KeyStore clientStore = KeyStore.getInstance(PKCS12);
			clientStore.load(new FileInputStream(keyFile), keyPass.toCharArray());

			KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
			kmf.init(clientStore, keyPass.toCharArray());
			KeyManager[] kms = kmf.getKeyManagers();

			SSLContext sslContext = SSLContext.getInstance(TLS);
			sslContext.init(kms, null, new SecureRandom());

			int argsSize = args.length;
			String mainArgumet;
			String dataTypeArgumet;
			String eventTypeArgumet;
			switch (argsSize) {
				// 'check'
				case 1:
					mainArgumet = args[MAIN_ARGUMENT_INDEX];
					if (CHECK_ARGUMENT.equals(mainArgumet)) {
						checkAuth(checkAuthUri, sslContext);
					} else {
						System.out.println("Not allowed arguments combination. Please, use 'help' argument for more information.");
					}
					break;
				// 'test wp' || 'test bin'
				case 2:
					mainArgumet = args[MAIN_ARGUMENT_INDEX];
					if (TEST_ARGUMENT.equals(mainArgumet)) {
						dataTypeArgumet = args[DATA_TYPE_ARGUMENT_INDEX];
						if (WAYPOINT_ARGUMENT.equals(dataTypeArgumet)) {
							testImportAPI(loadWaypointUri, WAYPOINT_SAMPLE, sslContext);
						} else if (BINARY_DATA_ARGUMENT.equals(dataTypeArgumet)){
							testImportAPI(loadBinaryDataUri, BINARY_DATA_SAMPLE, sslContext);
						} else if (EVENT_ARGUMENT.equals(dataTypeArgumet)){
							System.out.println("Not allowed to use 'ev' argument without third argument. Please, use 'help' argument for more information.");
						} else {
							System.out.println("Not allowed second argument: '" + dataTypeArgumet + "'. Please, use 'help' argument for more information.");
						}
					} else {
						System.out.println("Not allowed arguments combination. Please, use 'help' argument for more information.");
					}
					break;
				// 'test ev $type'
				case 3:
					mainArgumet = args[MAIN_ARGUMENT_INDEX];
					if (TEST_ARGUMENT.equals(mainArgumet)){
						dataTypeArgumet = args[DATA_TYPE_ARGUMENT_INDEX];
						if (EVENT_ARGUMENT.equals(dataTypeArgumet)) {
							eventTypeArgumet = args[EVENT_TYPE_ARGUMENT_INDEX];
							switch (eventTypeArgumet) {
								case ASSISTANCE_SWITCH_EVENT_ARGUMENT:
									testImportAPI(loadAssistanceSwitchEventUri, ASSISTANCE_SWITCH_EVENT_SAMPLE, sslContext);
									break;
								case BATTERY_EVENT_ARGUMENT:
									testImportAPI(loadBatteryEventUri, BATTERY_EVENT_SAMPLE, sslContext);
									break;
								case CHARGING_EVENT_ARGUMENT:
									testImportAPI(loadChargingEventUri, CHARGING_EVENT_SAMPLE, sslContext);
									break;
								case ENCLOSURE_TAMPER_EVENT_ARGUMENT:
									testImportAPI(loadEnclosureTamperEventUri, ENCLOSURE_TAMPER_EVENT_SAMPLE, sslContext);
									break;
								case RESET_EVENT_ARGUMENT:
									testImportAPI(loadResetEventUri, RESET_EVENT_SAMPLE, sslContext);
									break;
								case TAMPER_LOOP_CABLE_EVENT_ARGUMENT:
									testImportAPI(loadTamperLoopCableEventUri, TAMPER_LOOP_CABLE_EVENT_SAMPLE, sslContext);
									break;
								case RFID_EVENT_ARGUMENT:
									testImportAPI(loadRfidEventUri, RFID_EVENT_SAMPLE, sslContext);
									break;
								case BEACON_EVENT_ARGUMENT:
									testImportAPI(loadBeaconEventUri, BEACON_EVENT_SAMPLE, sslContext);
									break;
								case GPS_CONNECTIVITY_TIMEOUT_EVENT_ARGUMENT:
									testImportAPI(loadGpsConnectivityTimeoutEventUri, GPS_CONNECTIVITY_TIMEOUT_EVENT_SAMPLE, sslContext);
									break;
								case SATELLITE_CONNECTIVITY_TIMEOUT_EVENT_ARGUMENT:
									testImportAPI(loadSatelliteConnectivityTimeoutEventUri, SATELLITE_CONNECTIVITY_TIMEOUT_EVENT_SAMPLE, sslContext);
									break;
								case CELULLAR_CONNECTIVITY_TIMEOUT_EVENT_ARGUMENT:
									testImportAPI(loadCelullarConnectivityTimeoutEventUri, CELULLAR_CONNECTIVITY_TIMEOUT_EVENT_SAMPLE, sslContext);
									break;
								case ANTENNA_STATE_EVENT_ARGUMENT:
									testImportAPI(loadAntennaStateEventUri, ANTENNA_STATE_EVENT_SAMPLE, sslContext);
									break;
								default:
									System.out.println("Not allowed third argument: '" + eventTypeArgumet + "'. Please, use 'help' argument for more information.");
									break;
							}
						} else if (WAYPOINT_ARGUMENT.equals(dataTypeArgumet)){
							System.out.println("Not allowed to use 'wp' argument with third argument. Please, use 'help' argument for more information.");
						} else if (BINARY_DATA_ARGUMENT.equals(dataTypeArgumet)){
							System.out.println("Not allowed to use 'bin' argument with third argument. Please, use 'help' argument for more information.");
						} else {
							System.out.println("Not allowed second argument: '" + dataTypeArgumet + "'. Please, use 'help' argument for more information.");
						}
					} else {
						System.out.println("Not allowed first argument: '" + mainArgumet + "'. Please, use 'help' argument for more information.");
					}
					break;
				default:
					System.out.println("Invalid size of args! Please, use 'help' argument for more information.");
					break;
			}
		}
	}

	private static void checkAuth(URI uri, SSLContext sslContext) {
		try {
			var request = HttpRequest.newBuilder()
					.uri(uri)
					.header("Content-Type", "application/json")
					.GET()
					.build();
			var client = HttpClient.newBuilder().sslContext(sslContext).build();
			HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
			System.out.println("HTTP Response: " + response.toString());
			System.out.println("HTTP Response Body: " + response.body());
		} catch (Exception e) {
			System.err.println("Exception during API call. Message: " + e.getMessage());
		}
	}

	private static void testImportAPI(URI uri, String dataSamplePath, SSLContext sslContext) {
		try {
			// read file with data samples
			BufferedReader br = new BufferedReader(new FileReader(dataSamplePath));
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();
			while (line != null) {
				sb.append(line);
				line = br.readLine();
			}
			// prepare request bodies
			String string = sb.toString().substring(1, sb.toString().length() - 1);
			List<String> requestsToSend = Arrays.stream(string.split(DELIMITER)).map(body -> {
				char lastChar = body.charAt(body.length() - 1);
				if (COMMA == (lastChar)) {
					return body.substring(0, body.length() - 1);
				} else {
					return body;
				}
			}).collect(Collectors.toList());
			// send requests
			for (String requestBody : requestsToSend){
				var request = HttpRequest.newBuilder()
						.uri(uri)
						.header("Content-Type", "application/json")
						.POST(HttpRequest.BodyPublishers.ofString(requestBody))
						.build();
				var client = HttpClient.newBuilder().sslContext(sslContext).build();
				System.out.println("Test request sent. Date: " + new Date());
				int statusCode = ERROR_STATUS_CODE;
				while (statusCode == ERROR_STATUS_CODE) {
					HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
					System.out.println("HTTP Response: " + response.toString());
					System.out.println("HTTP Response Body: " + response.body());
					statusCode = response.statusCode();
				}
			}
		} catch (Exception e) {
			System.err.println("Exception during API testing. Message: " + e.getMessage());
		}
	}
}
