## Import API Client

This is root directory for Import API client:

- java/     directory contains Java 11 Import API client.
- keystore/ directory contains X.509 certificate file (.p12).
- sample/   directory contains .json files with data examples.
