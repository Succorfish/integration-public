import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Base64;
import java.util.Objects;
import java.util.Properties;

public class PullRunner {
	// Properties
	private static final String URL = "https://ws.scstg.net/jwt/v2/pull-api/get";
	private static final String TOKEN = "token";

	// conf files locations
	private static final String PROPERTIES_FILE = "application.properties";

	public static void main(String[] args) throws Exception {
		Properties properties = new Properties();
		// load a properties file from class path
		properties.load(Objects.requireNonNull(PullRunner.class.getResourceAsStream(PROPERTIES_FILE)));

		URI uri = URI.create(URL);
		String token = properties.getProperty(TOKEN);
		try {
			var request = HttpRequest.newBuilder()
					.uri(uri)
					.header("Content-Type", "application/json")
					.header("Authorization", bearerAuth(token))
					.GET()
					.build();
			var client = HttpClient.newBuilder().build();
			HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
			System.out.println("HTTP Response: " + response.toString());
			System.out.println("HTTP Response Body: " + response.body());
		} catch (Exception e) {
			System.err.println("Exception during API call. Message: " + e.getMessage());
		}
	}

	private static String bearerAuth(String token) {
		return "Bearer " + token;
	}

}
